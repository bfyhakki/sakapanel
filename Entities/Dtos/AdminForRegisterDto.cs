﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Dtos
{
   public class AdminForRegisterDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
    }
}
