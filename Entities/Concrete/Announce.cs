using System;
using System.ComponentModel.DataAnnotations;
using Core.Entities;

namespace Entities.Concrete
{
    public class Announce:IEntity
    {
        [Key] public int ANN_ID { get; set; }
        public string ANN_Title { get; set; }
        public string ANN_Content { get; set; }
        public string ANN_Photo { get; set; }
        public int ANN_Order { get; set; }
        public DateTime ANN_Date { get; set; }
        public int ANN_Status { get; set; }
    }
}