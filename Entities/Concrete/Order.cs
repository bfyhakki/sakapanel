using System;
using System.ComponentModel.DataAnnotations;
using Core.Entities;

namespace Entities.Concrete
{
    public class Order:IEntity
    {
        [Key] public long ORD_ID { get; set; }
        public long ORD_CustomerID { get; set; }
        public double ORD_Total { get; set; }
        public int ORD_PaymentType { get; set; }
        public string ORD_SakaRefID { get; set; }
        public string ORD_PaymentRefID { get; set; }
        public DateTime ORD_Date { get; set; }
        public string ORD_Message { get; set; }
        public int ORD_Status { get; set; }
    }
}