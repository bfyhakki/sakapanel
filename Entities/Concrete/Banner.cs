using System.ComponentModel.DataAnnotations;
using Core.Entities;

namespace Entities.Concrete
{
    public class Banner:IEntity
    {
        [Key]
        public int BNN_ID { get; set; }

        public string BNN_Image { get; set; }
        public string BNN_Link { get; set; }
        public int BNN_Order { get; set; }
        public int BNN_Status { get; set; }
    }
}