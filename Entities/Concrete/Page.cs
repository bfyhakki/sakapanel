using System.ComponentModel.DataAnnotations;
using Core.Entities;

namespace Entities.Concrete
{
    public class Page:IEntity
    {
        [Key] public int PG_ID { get; set; }
        public string PG_Title { get; set; }
        public string PG_Content { get; set; }
        public string PG_Photo { get; set; }
        public int PG_Status { get; set; }
        
    }
}