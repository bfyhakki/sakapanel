using System.ComponentModel.DataAnnotations;
using Core.Entities;

namespace Entities.Concrete
{
    public class Customer:IEntity
    {
        [Key] public long CST_ID { get; set; }
        public string CST_Phone { get; set; }
        public string CST_OtpPassword { get; set; }
        public string CST_Name { get; set; }
        public string CST_Email { get; set; }
        public int CST_Type { get; set; }
        public string CST_CustomerID { get; set; }
        public int CST_Status { get; set; }
    }
}