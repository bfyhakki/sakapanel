using System;
using System.ComponentModel.DataAnnotations;
using Core.Entities;

namespace Entities.Concrete
{
    public class ContactForm:IEntity
    {
        [Key] public int CF_ID { get; set; }
        public string CF_FullName { get; set; }
        public string CF_Email { get; set; }
        public string CF_Phone { get; set; }
        public string CF_Message { get; set; }
        public DateTime CF_Date { get; set; }
        public int CF_Type { get; set; }
        public int CF_Status { get; set; }
    }
}