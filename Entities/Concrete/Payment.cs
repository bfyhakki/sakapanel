using System.ComponentModel.DataAnnotations;
using Core.Entities;

namespace Entities.Concrete
{
    public class Payment:IEntity
    {
        [Key] public int PAY_ID { get; set; }
        public long PAY_CustomerID { get; set; }
        public string PAY_Token { get; set; }
        public string PAY_Userkey { get; set; }
        public int PAY_Status { get; set; }
    }
}