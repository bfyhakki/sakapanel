using System.ComponentModel.DataAnnotations;
using Core.Entities;

namespace Entities.Concrete
{
    public class Product:IEntity
    {
        [Key]
        public int PRD_ID { get; set; }

        public string PRD_Name { get; set; }
        public string PRD_Photo { get; set; }
        public string PRD_CatologPhoto { get; set; }
        public string PRD_Code { get; set; }
        public int PRD_Max_Bireysel { get; set; }
        public int PRD_Order { get; set; }
        public double PRD_Price { get; set; }
        public string PRD_Text { get; set; }
        public int PRD_Max_Kurumsal { get; set; }
        public int PRD_AllowOrder { get; set; }
        public int PRD_Type { get; set; }
        public int PRD_Status { get; set; }
    }
}