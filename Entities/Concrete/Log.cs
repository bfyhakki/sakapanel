using System;
using System.ComponentModel.DataAnnotations;
using Core.Entities;

namespace Entities.Concrete
{
    public class Log:IEntity
    {
        [Key] public long LOG_ID { get; set; }
        public string LOG_Gsm { get; set; }
        public string LOG_Data { get; set; }
        public DateTime LOG_Date { get; set; }
        public string LOG_IP { get; set; }
        public int LOG_Type { get; set; }
        public int LOG_Status { get; set; }
    }
}