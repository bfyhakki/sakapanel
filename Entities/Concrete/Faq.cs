using System.ComponentModel.DataAnnotations;

using Core.Entities;

namespace Entities.Concrete
{
    public class Faq:IEntity
    {
        [Key] public int FAQ_ID { get; set; }
        public string FAQ_Question { get; set; }
        public string FAQ_Answer { get; set; }
        public int FAQ_Order { get; set; }
        public int FAQ_Status { get; set; }
    }
}