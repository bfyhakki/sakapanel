﻿using Autofac;
using Autofac.Extras.DynamicProxy;
using Business.Abstract;
using Business.Concrete;
using Castle.DynamicProxy;
using Core.Utilities.Interceptors;
using Core.Utilities.Security.Jwt;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;
using hak=Castle.DynamicProxy.ProxyGenerationOptions;
namespace Business.DependencyResolvers.Autofac
{
   public class AutofacBusinessModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<OrderManager>().As<IOrderService>();
            builder.RegisterType<EfOrderDal>().As<IOrderDal>();

            builder.RegisterType<PaymentManager>().As<IPaymentService>();
            builder.RegisterType<EfPaymentDal>().As<IPaymentDal>();

            builder.RegisterType<CustomerManager>().As<ICustomerService>();
            builder.RegisterType<EfCustomerDal>().As<ICustomerDal>();

            
            builder.RegisterType<PageManager>().As<IPageService>();
            builder.RegisterType<EfPageDal>().As<IPageDal>();


            builder.RegisterType<ContactFormManager>().As<IContactFormService>();
            builder.RegisterType<EfContactFormDal>().As<IContactFormDal>();
            
            
         
            builder.RegisterType<LogManager>().As<ILogService>();
            builder.RegisterType<EfLogDal>().As<ILogDal>();


            builder.RegisterType<ProductManager>().As<IProductService>();
            builder.RegisterType<EfProductDal>().As<IProductDal>();

            
            
            builder.RegisterType<BannerManager>().As<IBannerService>();
            builder.RegisterType<EfBannerDal>().As<IBannerDal>();
            
            builder.RegisterType<FaqManager>().As<IFaqService>();
            builder.RegisterType<EfFaqDal>().As<IFaqDal>();
        
       
            builder.RegisterType<AnnounceManager>().As<IAnnounceService>();
            builder.RegisterType<EfAnnounceDal>().As<IAnnounceDal>();


            builder.RegisterType<AdminManager>().As<IAdminService>();
            builder.RegisterType<EfAdminDal>().As<IAdminDal>();

       



         

            //builder.RegisterType<AuthManager>().As<IAuthService>();
            builder.RegisterType<JwtHelper>().As<ITokenHelper>();

            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            builder.RegisterAssemblyTypes(assembly).AsImplementedInterfaces().EnableInterfaceInterceptors(new  ProxyGenerationOptions
                
      
            {

                Selector = new AspectInterceptorSelector()

            }).SingleInstance();



        }

    }
}
