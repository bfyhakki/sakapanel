﻿using Core.Utilities.Results;
using Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Abstract
{
   public interface IPaymentService
    {
        IDataResult<Payment> GetById(int bookId);
        IDataResult<List<Payment>>GetList();
        // IDataResult<List<Book>>GetListByPublisher(int PublisherId);
        // IDataResult<List<UserBookRead>> GetBookReads();
        // IDataResult<List<UserBookLike>> GetBookLikes();
        // IDataResult<List<Book>> GetListByCategory(int categoryId);
        // IDataResult<List<Book>> GetListByWriter(int categoryId);
        // IDataResult<List<Book>> GetCategoryListByBookId(int Id);
        // IResult UpdateBookWriter(int[] bookWriter, int bookId);
        // IResult UpdateBookCategories(int[] bookWriter, int bookId);
        // IResult UpdateBookTags(List<int> bookCategories, int bookId);
        IResult  Add(Payment book);
        IResult Update(Payment book);
        IResult Delete(Payment book);
        // IResult DeleteAll(int Id);


    }
}
