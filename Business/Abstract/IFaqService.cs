﻿using Core.Utilities.Results;
using Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Abstract
{
   public interface IFaqService
    {
        IDataResult<Faq> GetById(long bookId);
        IDataResult<List<Faq>>GetList();
        // IDataResult<List<Book>>GetListByPublisher(int PublisherId);
        // IDataResult<List<UserBookRead>> GetBookReads();
        // IDataResult<List<UserBookLike>> GetBookLikes();
        // IDataResult<List<Book>> GetListByCategory(int categoryId);
        // IDataResult<List<Book>> GetListByWriter(int categoryId);
        // IDataResult<List<Book>> GetCategoryListByBookId(int Id);
        // IResult UpdateBookWriter(int[] bookWriter, int bookId);
        // IResult UpdateBookCategories(int[] bookWriter, int bookId);
        // IResult UpdateBookTags(List<int> bookCategories, int bookId);
        IResult  Add(Faq book);
        IResult Update(Faq book);
        IResult Delete(Faq book);
        // IResult DeleteAll(int Id);


    }
}
