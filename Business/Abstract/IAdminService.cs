﻿using Core.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Abstract
{
   public interface IAdminService
    {
        // List<OperationClaim> GetClaims(Admin admin);
        List<Admin> GetList();
        void Add(Admin admin);
        Admin GetByEmail(string email);
        Admin GetByEmailAndPassword(string email,string password);
        Admin GetById(int Id);
      
    }
}
