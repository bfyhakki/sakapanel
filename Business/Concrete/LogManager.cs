﻿using Business.Abstract;
using Business.BusinessAspect.Autofac;
using Business.Constant;
using Business.ValidationRules.FluentValidation;
using Business.ValidationRules.FluentValidation.LogValidation;
using Core.Aspects.Autofac.Caching;
using Core.Aspects.Autofac.Validation;
using Core.Aspects.Transaction;
using Core.CrossCuttingConcerns.Validations.FluentValidation;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Concrete
{
    public class LogManager : ILogService
    {
        private ILogDal _LogDal;



        public LogManager(  ILogDal LogDal)
        {
            _LogDal = LogDal;
          
        }

        //[ValidationAspect(typeof(LogValidator), Priority = 1)]
        [TransactionScopeAspect]
        [CacheRemoveAspect("ILogService.Get")]

        public IResult Add(Log Log)
        {
          
            var validator = new LogValidator();
            var validationResult = validator.Validate(Log);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Log>("hata", validationResult);
            }

            else
            {
             


                _LogDal.Add(Log);
                return new SuccessResult(Messages.Added);
            }
          
        }
        [CacheRemoveAspect("ILogService.Get")]
        public IResult Delete(Log Log)
        {
            Log.LOG_Status = 0;
            _LogDal.Update(Log);

            return new SuccessResult(Messages.Delete);
        }
  


        public IDataResult<Log> GetById(int LogId)
        {

            return new SuccessDataResult<Log>(_LogDal.Get(x => x.LOG_ID == LogId));
        }
        //[SecuredOperation("Admin,Log.List")]
        [CacheAspect]
        public  IDataResult<List<Log>> GetList()
        {
            
            return new SuccessDataResult<List<Log>>(_LogDal.GetList(x=> x.LOG_Status> -1).ToList());
        }
      
        [CacheRemoveAspect("ILogService.Get")]
        public IResult Update(Log Log)
        {
            var validator = new LogValidator();
            var validationResult = validator.Validate(Log);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Log>("hata", validationResult);
            }

            else
            {
            


                _LogDal.Update(Log);
                return new SuccessResult(Messages.Added);
            }
           
       
        }

        //Log audio and video




        //[TransactionScopeAspect]
        //public IResult test(Log Log)
        //{
        //    _LogDal.Update(Log);
        //    _LogDal.Add(Log);
        //    return new SuccessResult(Messages.Update);
        //}
    }
}
