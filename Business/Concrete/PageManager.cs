﻿using Business.Abstract;
using Business.BusinessAspect.Autofac;
using Business.Constant;
using Business.ValidationRules.FluentValidation;
using Business.ValidationRules.FluentValidation.PageValidation;
using Core.Aspects.Autofac.Caching;
using Core.Aspects.Autofac.Validation;
using Core.Aspects.Transaction;
using Core.CrossCuttingConcerns.Validations.FluentValidation;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Concrete
{
    public class PageManager : IPageService
    {
        private IPageDal _PageDal;



        public PageManager(  IPageDal PageDal)
        {
            _PageDal = PageDal;
          
        }

        //[ValidationAspect(typeof(PageValidator), Priority = 1)]
        [TransactionScopeAspect]
        [CacheRemoveAspect("IPageService.Get")]

        public IResult Add(Page Page)
        {
          
            var validator = new PageValidator();
            var validationResult = validator.Validate(Page);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Page>("hata", validationResult);
            }

            else
            {
             


                _PageDal.Add(Page);
                return new SuccessResult(Messages.Added);
            }
          
        }
        [CacheRemoveAspect("IPageService.Get")]
        public IResult Delete(Page Page)
        {
            Page.PG_Status = 0;
            _PageDal.Update(Page);

            return new SuccessResult(Messages.Delete);
        }
  


        public IDataResult<Page> GetById(int PageId)
        {

            return new SuccessDataResult<Page>(_PageDal.Get(x => x.PG_ID == PageId));
        }
        //[SecuredOperation("Admin,Page.List")]
        [CacheAspect]
        public  IDataResult<List<Page>> GetList()
        {
            
            return new SuccessDataResult<List<Page>>(_PageDal.GetList(x=> x.PG_Status> -1).ToList());
        }
      
        [CacheRemoveAspect("IPageService.Get")]
        public IResult Update(Page Page)
        {
            var validator = new PageValidator();
            var validationResult = validator.Validate(Page);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Page>("hata", validationResult);
            }

            else
            {
            


                _PageDal.Update(Page);
                return new SuccessResult(Messages.Added);
            }
           
       
        }

        //Page audio and video




        //[TransactionScopeAspect]
        //public IResult test(Page Page)
        //{
        //    _PageDal.Update(Page);
        //    _PageDal.Add(Page);
        //    return new SuccessResult(Messages.Update);
        //}
    }
}
