﻿using Business.Abstract;
using Business.BusinessAspect.Autofac;
using Business.Constant;
using Business.ValidationRules.FluentValidation;
using Business.ValidationRules.FluentValidation.ProductValidation;
using Core.Aspects.Autofac.Caching;
using Core.Aspects.Autofac.Validation;
using Core.Aspects.Transaction;
using Core.CrossCuttingConcerns.Validations.FluentValidation;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Concrete
{
    public class ProductManager : IProductService
    {
        private IProductDal _ProductDal;



        public ProductManager(  IProductDal ProductDal)
        {
            _ProductDal = ProductDal;
          
        }

        //[ValidationAspect(typeof(ProductValidator), Priority = 1)]
        [TransactionScopeAspect]
        [CacheRemoveAspect("IProductService.Get")]

        public IResult Add(Product Product)
        {
          
            var validator = new ProductValidator();
            var validationResult = validator.Validate(Product);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Product>("hata", validationResult);
            }

            else
            {
             


                _ProductDal.Add(Product);
                return new SuccessResult(Messages.Added);
            }
          
        }
        [CacheRemoveAspect("IProductService.Get")]
        public IResult Delete(Product Product)
        {
            Product.PRD_Status = 0;
            _ProductDal.Update(Product);

            return new SuccessResult(Messages.Delete);
        }
  


        public IDataResult<Product> GetById(int ProductId)
        {

            return new SuccessDataResult<Product>(_ProductDal.Get(x => x.PRD_ID == ProductId));
        }
        //[SecuredOperation("Admin,Product.List")]
        [CacheAspect]
        public  IDataResult<List<Product>> GetList()
        {
            
            return new SuccessDataResult<List<Product>>(_ProductDal.GetList(x=> x.PRD_Status> -1).ToList());
        }
      
        [CacheRemoveAspect("IProductService.Get")]
        public IResult Update(Product Product)
        {
            var validator = new ProductValidator();
            var validationResult = validator.Validate(Product);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Product>("hata", validationResult);
            }

            else
            {
            


                _ProductDal.Update(Product);
                return new SuccessResult(Messages.Added);
            }
           
       
        }

        //Product audio and video




        //[TransactionScopeAspect]
        //public IResult test(Product Product)
        //{
        //    _ProductDal.Update(Product);
        //    _ProductDal.Add(Product);
        //    return new SuccessResult(Messages.Update);
        //}
    }
}
