﻿using Business.Abstract;
using Business.BusinessAspect.Autofac;
using Business.Constant;
using Business.ValidationRules.FluentValidation;
using Business.ValidationRules.FluentValidation.FaqValidation;
using Core.Aspects.Autofac.Caching;
using Core.Aspects.Autofac.Validation;
using Core.Aspects.Transaction;
using Core.CrossCuttingConcerns.Validations.FluentValidation;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Concrete
{
    public class FaqManager : IFaqService
    {
        private IFaqDal _FaqDal;



        public FaqManager(  IFaqDal FaqDal)
        {
            _FaqDal = FaqDal;
          
        }

        //[ValidationAspect(typeof(FaqValidator), Priority = 1)]
        [TransactionScopeAspect]
        [CacheRemoveAspect("IFaqService.Get")]

        public IResult Add(Faq Faq)
        {
          
            var validator = new FaqValidator();
            var validationResult = validator.Validate(Faq);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Faq>("hata", validationResult);
            }

            else
            {
             


                _FaqDal.Add(Faq);
                return new SuccessResult(Messages.Added);
            }
          
        }
        [CacheRemoveAspect("IFaqService.Get")]
        public IResult Delete(Faq Faq)
        {
            Faq.FAQ_Status = 0;
            _FaqDal.Update(Faq);

            return new SuccessResult(Messages.Delete);
        }
  


        public IDataResult<Faq> GetById(long FaqId)
        {

            return new SuccessDataResult<Faq>(_FaqDal.Get(x => x.FAQ_ID == FaqId));
        }
        //[SecuredOperation("Admin,Faq.List")]
        [CacheAspect]
        public  IDataResult<List<Faq>> GetList()
        {
            
            return new SuccessDataResult<List<Faq>>(_FaqDal.GetList(x=> x.FAQ_Status> -1).ToList());
        }
      
        [CacheRemoveAspect("IFaqService.Get")]
        public IResult Update(Faq Faq)
        {
            var validator = new FaqValidator();
            var validationResult = validator.Validate(Faq);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Faq>("hata", validationResult);
            }

            else
            {
            


                _FaqDal.Update(Faq);
                return new SuccessResult(Messages.Added);
            }
           
       
        }

        //Faq audio and video




        //[TransactionScopeAspect]
        //public IResult test(Faq Faq)
        //{
        //    _FaqDal.Update(Faq);
        //    _FaqDal.Add(Faq);
        //    return new SuccessResult(Messages.Update);
        //}
    }
}
