﻿using Business.Abstract;
using Business.BusinessAspect.Autofac;
using Business.Constant;
using Business.ValidationRules.FluentValidation;
using Business.ValidationRules.FluentValidation.ContactFormValidation;
using Core.Aspects.Autofac.Caching;
using Core.Aspects.Autofac.Validation;
using Core.Aspects.Transaction;
using Core.CrossCuttingConcerns.Validations.FluentValidation;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Concrete
{
    public class ContactFormManager : IContactFormService
    {
        private IContactFormDal _ContactFormDal;



        public ContactFormManager(  IContactFormDal ContactFormDal)
        {
            _ContactFormDal = ContactFormDal;
          
        }

        //[ValidationAspect(typeof(ContactFormValidator), Priority = 1)]
        [TransactionScopeAspect]
        [CacheRemoveAspect("IContactFormService.Get")]

        public IResult Add(ContactForm ContactForm)
        {
          
            var validator = new ContactFormValidator();
            var validationResult = validator.Validate(ContactForm);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<ContactForm>("hata", validationResult);
            }

            else
            {
             


                _ContactFormDal.Add(ContactForm);
                return new SuccessResult(Messages.Added);
            }
          
        }
        [CacheRemoveAspect("IContactFormService.Get")]
        public IResult Delete(ContactForm ContactForm)
        {
            ContactForm.CF_Status = 0;
            _ContactFormDal.Update(ContactForm);

            return new SuccessResult(Messages.Delete);
        }
  


        public IDataResult<ContactForm> GetById(int ContactFormId)
        {

            return new SuccessDataResult<ContactForm>(_ContactFormDal.Get(x => x.CF_ID == ContactFormId));
        }
        //[SecuredOperation("Admin,ContactForm.List")]
        [CacheAspect]
        public  IDataResult<List<ContactForm>> GetList()
        {
            
            return new SuccessDataResult<List<ContactForm>>(_ContactFormDal.GetList(x=> x.CF_Status> -1).ToList());
        }
      
        [CacheRemoveAspect("IContactFormService.Get")]
        public IResult Update(ContactForm ContactForm)
        {
            var validator = new ContactFormValidator();
            var validationResult = validator.Validate(ContactForm);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<ContactForm>("hata", validationResult);
            }

            else
            {
            


                _ContactFormDal.Update(ContactForm);
                return new SuccessResult(Messages.Added);
            }
           
       
        }

        //ContactForm audio and video




        //[TransactionScopeAspect]
        //public IResult test(ContactForm ContactForm)
        //{
        //    _ContactFormDal.Update(ContactForm);
        //    _ContactFormDal.Add(ContactForm);
        //    return new SuccessResult(Messages.Update);
        //}
    }
}
