﻿using Business.Abstract;
using Business.BusinessAspect.Autofac;
using Business.Constant;
using Business.ValidationRules.FluentValidation;
using Business.ValidationRules.FluentValidation.BannerValidation;
using Core.Aspects.Autofac.Caching;
using Core.Aspects.Autofac.Validation;
using Core.Aspects.Transaction;
using Core.CrossCuttingConcerns.Validations.FluentValidation;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Concrete
{
    public class BannerManager : IBannerService
    {
        private IBannerDal _BannerDal;



        public BannerManager(  IBannerDal bannerDal)
        {
            _BannerDal = bannerDal;
          
        }

        //[ValidationAspect(typeof(BannerValidator), Priority = 1)]
        [TransactionScopeAspect]
        [CacheRemoveAspect("IBannerService.Get")]

        public IResult Add(Banner Banner)
        {
          
            var validator = new BannerValidator();
            var validationResult = validator.Validate(Banner);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Banner>("hata", validationResult);
            }

            else
            {
             


                _BannerDal.Add(Banner);
                return new SuccessResult(Messages.Added);
            }
          
        }
        [CacheRemoveAspect("IBannerService.Get")]
        public IResult Delete(Banner Banner)
        {
            Banner.BNN_Status = 0;
            _BannerDal.Update(Banner);

            return new SuccessResult(Messages.Delete);
        }
  


        public IDataResult<Banner> GetById(int BannerId)
        {

            return new SuccessDataResult<Banner>(_BannerDal.Get(x => x.BNN_ID == BannerId));
        }
        //[SecuredOperation("Admin,Banner.List")]
        [CacheAspect]
        public  IDataResult<List<Banner>> GetList()
        {
            
            return new SuccessDataResult<List<Banner>>(_BannerDal.GetList(x=> x.BNN_Status> -1).ToList());
        }
      
        [CacheRemoveAspect("IBannerService.Get")]
        public IResult Update(Banner Banner)
        {
            var validator = new BannerValidator();
            var validationResult = validator.Validate(Banner);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Banner>("hata", validationResult);
            }

            else
            {
            


                _BannerDal.Update(Banner);
                return new SuccessResult(Messages.Added);
            }
           
       
        }

        //Banner audio and video




        //[TransactionScopeAspect]
        //public IResult test(Banner Banner)
        //{
        //    _BannerDal.Update(Banner);
        //    _BannerDal.Add(Banner);
        //    return new SuccessResult(Messages.Update);
        //}
    }
}
