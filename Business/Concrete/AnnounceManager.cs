﻿using Business.Abstract;
using Business.BusinessAspect.Autofac;
using Business.Constant;
using Business.ValidationRules.FluentValidation;
using Business.ValidationRules.FluentValidation.AnnounceValidation;
using Core.Aspects.Autofac.Caching;
using Core.Aspects.Autofac.Validation;
using Core.Aspects.Transaction;
using Core.CrossCuttingConcerns.Validations.FluentValidation;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Concrete
{
    public class AnnounceManager : IAnnounceService
    {
        private IAnnounceDal _AnnounceDal;



        public AnnounceManager(  IAnnounceDal AnnounceDal)
        {
            _AnnounceDal = AnnounceDal;
          
        }

        //[ValidationAspect(typeof(AnnounceValidator), Priority = 1)]
        [TransactionScopeAspect]
        [CacheRemoveAspect("IAnnounceService.Get")]

        public IResult Add(Announce Announce)
        {
          
            var validator = new AnnounceValidator();
            var validationResult = validator.Validate(Announce);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Announce>("hata", validationResult);
            }

            else
            {
             


                _AnnounceDal.Add(Announce);
                return new SuccessResult(Messages.Added);
            }
          
        }
        [CacheRemoveAspect("IAnnounceService.Get")]
        public IResult Delete(Announce Announce)
        {
            Announce.ANN_Status = 0;
            _AnnounceDal.Update(Announce);

            return new SuccessResult(Messages.Delete);
        }
  


        public IDataResult<Announce> GetById(long AnnounceId)
        {

            return new SuccessDataResult<Announce>(_AnnounceDal.Get(x => x.ANN_ID == AnnounceId));
        }
        //[SecuredOperation("Admin,Announce.List")]
        [CacheAspect]
        public  IDataResult<List<Announce>> GetList()
        {
            
            return new SuccessDataResult<List<Announce>>(_AnnounceDal.GetList(x=> x.ANN_Status> -1).ToList());
        }
      
        [CacheRemoveAspect("IAnnounceService.Get")]
        public IResult Update(Announce Announce)
        {
            var validator = new AnnounceValidator();
            var validationResult = validator.Validate(Announce);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Announce>("hata", validationResult);
            }

            else
            {
            


                _AnnounceDal.Update(Announce);
                return new SuccessResult(Messages.Added);
            }
           
       
        }

        //Announce audio and video




        //[TransactionScopeAspect]
        //public IResult test(Announce Announce)
        //{
        //    _AnnounceDal.Update(Announce);
        //    _AnnounceDal.Add(Announce);
        //    return new SuccessResult(Messages.Update);
        //}
    }
}
