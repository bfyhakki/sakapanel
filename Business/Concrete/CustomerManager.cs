﻿using Business.Abstract;
using Business.BusinessAspect.Autofac;
using Business.Constant;
using Business.ValidationRules.FluentValidation;
using Business.ValidationRules.FluentValidation.CustomerValidation;
using Core.Aspects.Autofac.Caching;
using Core.Aspects.Autofac.Validation;
using Core.Aspects.Transaction;
using Core.CrossCuttingConcerns.Validations.FluentValidation;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Concrete
{
    public class CustomerManager : ICustomerService
    {
        private ICustomerDal _CustomerDal;



        public CustomerManager(  ICustomerDal CustomerDal)
        {
            _CustomerDal = CustomerDal;
          
        }

        //[ValidationAspect(typeof(CustomerValidator), Priority = 1)]
        [TransactionScopeAspect]
        [CacheRemoveAspect("ICustomerService.Get")]

        public IResult Add(Customer Customer)
        {
          
            var validator = new CustomerValidator();
            var validationResult = validator.Validate(Customer);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Customer>("hata", validationResult);
            }

            else
            {
             


                _CustomerDal.Add(Customer);
                return new SuccessResult(Messages.Added);
            }
          
        }
        [CacheRemoveAspect("ICustomerService.Get")]
        public IResult Delete(Customer Customer)
        {
            Customer.CST_Status = 0;
            _CustomerDal.Update(Customer);

            return new SuccessResult(Messages.Delete);
        }
  


        public IDataResult<Customer> GetById(long CustomerId)
        {

            return new SuccessDataResult<Customer>(_CustomerDal.Get(x => x.CST_ID == CustomerId));
        }
        //[SecuredOperation("Admin,Customer.List")]
        [CacheAspect]
        public  IDataResult<List<Customer>> GetList()
        {
            
            return new SuccessDataResult<List<Customer>>(_CustomerDal.GetList(x=> x.CST_Status> -1).ToList());
        }
      
        [CacheRemoveAspect("ICustomerService.Get")]
        public IResult Update(Customer Customer)
        {
            var validator = new CustomerValidator();
            var validationResult = validator.Validate(Customer);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Customer>("hata", validationResult);
            }

            else
            {
            


                _CustomerDal.Update(Customer);
                return new SuccessResult(Messages.Added);
            }
           
       
        }

        //Customer audio and video




        //[TransactionScopeAspect]
        //public IResult test(Customer Customer)
        //{
        //    _CustomerDal.Update(Customer);
        //    _CustomerDal.Add(Customer);
        //    return new SuccessResult(Messages.Update);
        //}
    }
}
