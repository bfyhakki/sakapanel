﻿using Business.Abstract;
using Business.BusinessAspect.Autofac;
using Business.Constant;
using Business.ValidationRules.FluentValidation;
using Business.ValidationRules.FluentValidation.OrderValidation;
using Core.Aspects.Autofac.Caching;
using Core.Aspects.Autofac.Validation;
using Core.Aspects.Transaction;
using Core.CrossCuttingConcerns.Validations.FluentValidation;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Concrete
{
    public class OrderManager : IOrderService
    {
        private IOrderDal _OrderDal;



        public OrderManager(  IOrderDal OrderDal)
        {
            _OrderDal = OrderDal;
          
        }

        //[ValidationAspect(typeof(OrderValidator), Priority = 1)]
        [TransactionScopeAspect]
        [CacheRemoveAspect("IOrderService.Get")]

        public IResult Add(Order Order)
        {
          
            var validator = new OrderValidator();
            var validationResult = validator.Validate(Order);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Order>("hata", validationResult);
            }

            else
            {
             


                _OrderDal.Add(Order);
                return new SuccessResult(Messages.Added);
            }
          
        }
        [CacheRemoveAspect("IOrderService.Get")]
        public IResult Delete(Order Order)
        {
            Order.ORD_Status = 0;
            _OrderDal.Update(Order);

            return new SuccessResult(Messages.Delete);
        }
  


        public IDataResult<Order> GetById(int OrderId)
        {

            return new SuccessDataResult<Order>(_OrderDal.Get(x => x.ORD_ID == OrderId));
        }
        //[SecuredOperation("Admin,Order.List")]
        [CacheAspect]
        public  IDataResult<List<Order>> GetList()
        {
            
            return new SuccessDataResult<List<Order>>(_OrderDal.GetList(x=> x.ORD_Status> -1).ToList());
        }
      
        [CacheRemoveAspect("IOrderService.Get")]
        public IResult Update(Order Order)
        {
            var validator = new OrderValidator();
            var validationResult = validator.Validate(Order);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Order>("hata", validationResult);
            }

            else
            {
            


                _OrderDal.Update(Order);
                return new SuccessResult(Messages.Added);
            }
           
       
        }

        //Order audio and video




        //[TransactionScopeAspect]
        //public IResult test(Order Order)
        //{
        //    _OrderDal.Update(Order);
        //    _OrderDal.Add(Order);
        //    return new SuccessResult(Messages.Update);
        //}
    }
}
