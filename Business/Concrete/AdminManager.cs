﻿using Business.Abstract;
using DataAccess.Abstract;
using Core.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Concrete
{
    public class AdminManager : IAdminService
    {
        IAdminDal _adminDal;
        public AdminManager(IAdminDal adminDal)
        {
            _adminDal = adminDal;

        }
        public void Add(Admin admin)
        {
            _adminDal.Add(admin);
        }
       
        public Admin GetByEmail(string email)
        {
           return _adminDal.Get(x => x.AD_Email == email);
        }
        public Admin GetByEmailAndPassword(string email,string pass)
        {
            return _adminDal.Get(x => x.AD_Email == email&&x.AD_Password==pass);
        }
        public Admin GetById(int Id)
        {
            return _adminDal.Get(x => x.AD_ID == Id);
        }

        // public List<OperationClaim> GetClaims(Admin admin)
        // {
        //     return _adminDal.GetClaims(admin);
        // }
        public List<Admin> GetList()
        {
            return _adminDal.GetList();
        }
    }
}
