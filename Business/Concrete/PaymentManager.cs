﻿using Business.Abstract;
using Business.BusinessAspect.Autofac;
using Business.Constant;
using Business.ValidationRules.FluentValidation;
using Business.ValidationRules.FluentValidation.PaymentValidation;
using Core.Aspects.Autofac.Caching;
using Core.Aspects.Autofac.Validation;
using Core.Aspects.Transaction;
using Core.CrossCuttingConcerns.Validations.FluentValidation;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Concrete
{
    public class PaymentManager : IPaymentService
    {
        private IPaymentDal _PaymentDal;



        public PaymentManager(  IPaymentDal PaymentDal)
        {
            _PaymentDal = PaymentDal;
          
        }

        //[ValidationAspect(typeof(PaymentValidator), Priority = 1)]
        [TransactionScopeAspect]
        [CacheRemoveAspect("IPaymentService.Get")]

        public IResult Add(Payment Payment)
        {
          
            var validator = new PaymentValidator();
            var validationResult = validator.Validate(Payment);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Payment>("hata", validationResult);
            }

            else
            {
             


                _PaymentDal.Add(Payment);
                return new SuccessResult(Messages.Added);
            }
          
        }
        [CacheRemoveAspect("IPaymentService.Get")]
        public IResult Delete(Payment Payment)
        {
            Payment.PAY_Status = 0;
            _PaymentDal.Update(Payment);

            return new SuccessResult(Messages.Delete);
        }
  


        public IDataResult<Payment> GetById(int PaymentId)
        {

            return new SuccessDataResult<Payment>(_PaymentDal.Get(x => x.PAY_ID == PaymentId));
        }
        //[SecuredOperation("Admin,Payment.List")]
        [CacheAspect]
        public  IDataResult<List<Payment>> GetList()
        {
            
            return new SuccessDataResult<List<Payment>>(_PaymentDal.GetList(x=> x.PAY_Status> -1).ToList());
        }
      
        [CacheRemoveAspect("IPaymentService.Get")]
        public IResult Update(Payment Payment)
        {
            var validator = new PaymentValidator();
            var validationResult = validator.Validate(Payment);
            if (!validationResult.IsValid)
            {
                return new ErrorDataResult<Payment>("hata", validationResult);
            }

            else
            {
            


                _PaymentDal.Update(Payment);
                return new SuccessResult(Messages.Added);
            }
           
       
        }

        //Payment audio and video




        //[TransactionScopeAspect]
        //public IResult test(Payment Payment)
        //{
        //    _PaymentDal.Update(Payment);
        //    _PaymentDal.Add(Payment);
        //    return new SuccessResult(Messages.Update);
        //}
    }
}
