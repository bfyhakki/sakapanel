﻿using Core.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Business.Constant
{
    public static class Messages
    {
        public static string Added = "Kayıt edildi.";
        public static string Update = "Kayıt güncellendi.";
        public static string Delete = "Kayıt silindi.";
        public static string Success = "İşlem başarılı.";
        public static string UserNotFound = "Kullanıcı bulunamadı.";
        public static string PasswordError="Şifre hatalı.";
        public static string SuccessfulLogin = "Login işlemi başarılı.";
        public static string UserAlreadyExists = "Bu kullanıcı zaten mevcut";
        public static string AdminRegistered = "Kullanıcı başarıyla kayıt edildi";
        public static string AccessTokenCreated = "Access Token oluşturuldu.";
        public static string AuthrozationDenied = "Auth olamadın ";

    
    }
}
