using Core.Entities.Concrete;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation.Resources;
using Admin = Core.Entities.Concrete.Admin;

namespace DataAccess.Concrete.EntityFramework.Contexts
{
   public class DashboardContext:DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=bfydigital.com;Database=DB_SAKA2020;User Id=DB_SAKA2020;Password=154190Aa;");
            // optionsBuilder.UseSqlServer(@"Server=virguldijital\SQLEXPRESS;Database=db_ebook;User Id=db_ebookuser;Password=r29mR_8k;");
         //   optionsBuilder.UseSqlServer(@"Server=boodiosql.database.windows.net;Database=db_ebook;User Id=db_ebook;Password=K89n_sk22;");
           
        }

        public DbSet<Admin> AD_ADMIN { get; set; }
        public DbSet<Banner> BNN_BANNER { get; set; }
        public DbSet<Product> PRD_Product { get; set; }
        public DbSet<Log> LOG_LOG { get; set; }
        public DbSet<Customer> CST_CUSTOMER { get; set; }
        public DbSet<Order> ORD_ORDER { get; set; }
        public DbSet<Payment> PAY_PAYMENT { get; set; }
        public DbSet<Page> PG_PAGE { get; set; }
        public DbSet<ContactForm> CF_CONTACTFORM { get; set; }
        public DbSet<Announce> ANN_ANNOUNCE { get; set; }
        public DbSet<Faq> FAQ_QUESTION { get; set; }
      


    }
}
