﻿using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Core.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace DataAccess.Concrete.EntityFramework
{
    public class EfAdminDal : EfEntityRepositoryBase<Admin, DashboardContext>, IAdminDal
    {
        // public List<OperationClaim> GetClaims(Admin admin)
        // {
        //     using (var context = new DashboardContext())
        //     {
        //         var result = from operationClaim in context.OC_OPERATIONCLAIM
        //                      join userOperationClaim in context.AOC_ADMINOPERATIONCLAIM 
        //                      on operationClaim.OC_ID equals userOperationClaim.AOC_OperationClaimID
        //                      where userOperationClaim.AOC_AdminID == admin.AD_ID
        //
        //                      select new OperationClaim { OC_ID = operationClaim.OC_ID, OC_Name = operationClaim.OC_Name };
        //         return result.ToList();
        //     }
        // }

        public List<Admin> GetList()
        {
            using (DashboardContext context= new DashboardContext())
            {
                return context.AD_ADMIN.ToList();
            }
        }
    }
}
