﻿using Core.DataAccess;
using Core.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Abstract
{
    public interface IAdminDal:IEntityRepository<Admin>
    {
        // List<OperationClaim> GetClaims(Admin admin);
        List<Admin> GetList();
      
        
    }
}
