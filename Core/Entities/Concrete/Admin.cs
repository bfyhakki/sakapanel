﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Entities.Concrete
{
   public class Admin  :IEntity
    {
        [Key]
        public int AD_ID { get; set; }
     
        public string AD_FirstName { get; set; }
        public string AD_Surname { get; set; }
        public string AD_Email { get; set; }
        public string AD_Password { get; set; }
     
        public int AD_Status { get; set; }
    }
}
