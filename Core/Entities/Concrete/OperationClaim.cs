﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Entities.Concrete
{
   public class OperationClaim:IEntity
    {
        [Key]
        public int OC_ID { get; set; }
        public string OC_Name { get; set; }
        public int OC_Status { get; set; }
    }
}
