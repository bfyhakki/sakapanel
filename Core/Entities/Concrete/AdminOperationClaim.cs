﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Entities.Concrete
{
    public class AdminOperationClaim:IEntity
    {
        [Key]
        public int AOC_ID { get; set; }
        public int AOC_AdminID { get; set; }
        public int AOC_OperationClaimID { get; set; }
        public int AOC_Status { get; set; }
    }
}
