﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Utilities.Results
{
    public class Result : IResult
    {
        //birden fazla ctor yazılabilir
        public Result(bool success,string message):this(success)
        {
            Message = message;
        }
        public Result(bool success, ValidationResult validationResult) : this(success)

        {

            ValidationResult = validationResult;

        }

        public Result(bool success, string message, ValidationResult validationResult) : this(success)

        {

            ValidationResult = validationResult;

            Message = message;

        }


        public Result(bool success)
        {
            Success = success;
        }
        public bool Success { get; }

        public string Message { get; }

        public ValidationResult ValidationResult { get; }
    }
}
