﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Utilities.Results
{
    public class DataResult<T> : Result, IDataResult<T>
    {
       public DataResult(T data,bool success,string message):base (success,message)
        {
            Data = data;

        }
        public DataResult(T data,bool success):base(success)
        {
            Data = data;

        }
        public DataResult(bool success, string message) : base(success, message)

        {

        }



        public DataResult(bool success) : base(success)

        {

        }



        public DataResult(bool success, ValidationResult errorResult) : base(success)

        {

            ValidationResult = errorResult;

        }



        public DataResult(bool success, string message, ValidationResult errorResult) : base(success, message)

        {

            ValidationResult = errorResult;

        }


        public T Data { get; }
        public new ValidationResult ValidationResult { get; }
    }
}
