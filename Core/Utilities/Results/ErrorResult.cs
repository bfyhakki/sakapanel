﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Utilities.Results
{
    public class ErrorResult:Result
    {
        public ErrorResult(string message) : base(false, message)
        {

        }
        public ErrorResult() : base(false)
        {

        }
        public ErrorResult(ValidationResult validationResult) : base(false, validationResult)

        {

        }

        public ErrorResult(string message, ValidationResult validationResult) : base(false, message, validationResult)

        {

        }
    }
}
