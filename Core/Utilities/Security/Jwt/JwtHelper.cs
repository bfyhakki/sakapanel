﻿using Core.Entities.Concrete;
using Core.Extentions;
using Core.Utilities.Security.Encrytion;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Core.Utilities.Security.Jwt
{
    public class JwtHelper : ITokenHelper
    {
        public IConfiguration Configuration { get;  }
        private TokenOptions _tokenOptions;
        DateTime _accessTokenExpiration;
        public JwtHelper(IConfiguration configuration)
        {
            Configuration = configuration;
            _tokenOptions = Configuration.GetSection("TokenOptions").Get<TokenOptions>();
           
        }
        public AccessToken CreateToken(Admin admin, List<OperationClaim> operationClaims)
        {
            _accessTokenExpiration = DateTime.Now.AddMinutes(_tokenOptions.AccessTokenExpiration);
            var securityKey = SecurityKeyHelper.CreateSecurityKey(_tokenOptions.SecurityKey);
            var signingCredentials = SigningCredentialsHelper.CreateSigningCredentials(securityKey);
            var jwt = CreateJwtSecurityToken(_tokenOptions, admin, signingCredentials, operationClaims);
            var jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            var token = jwtSecurityTokenHandler.WriteToken(jwt);


            return new AccessToken
            {
                Token = token,
                Expiration = _accessTokenExpiration
            };

        }
        public JwtSecurityToken CreateJwtSecurityToken(TokenOptions tokenOptions, Admin admin, SigningCredentials signingCredentials, List<OperationClaim> operationClaims)
        {
            var jwt = new JwtSecurityToken(
                
                issuer:tokenOptions.Issuer,
                audience:tokenOptions.Audience,
                expires: _accessTokenExpiration,
                notBefore:DateTime.Now,
                claims:SetClaims(admin,operationClaims),
                signingCredentials:signingCredentials
                
                );
            return jwt;

        }
        public IEnumerable<Claim> SetClaims(Admin admin,List<OperationClaim>operationClaims)
        {
            var claims = new List<Claim>();
            claims.Add(new Claim("email", admin.AD_Email));
            claims.AddNameIdentifier(admin.AD_ID.ToString());
            claims.AddEmail(admin.AD_Email.ToString());
            claims.AddName($"{admin.AD_FirstName}{admin.AD_Surname}");
            claims.AddRoles(operationClaims.Select(c => c.OC_Name).ToArray());
            return claims;

        }
    }
}
