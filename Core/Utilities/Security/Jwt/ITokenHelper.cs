﻿using Core.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Utilities.Security.Jwt
{
   public interface ITokenHelper
    {
        AccessToken CreateToken(Admin admin,List<OperationClaim>operationClaims);
    }
}
