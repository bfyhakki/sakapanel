﻿using Business.Abstract;
using Core.Entities.Concrete;
using Dashboard.Models;
using Dashboard.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.ViewComponents
{
    public class LoginSideViewComponent: ViewComponent
    {
        IAdminService _service;


        CookieManager manager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public LoginSideViewComponent(IAdminService service, IHttpContextAccessor contextAccessor)
        {
            _service = service;
            this._httpContextAccessor = contextAccessor;
            manager = new CookieManager(this._httpContextAccessor, service);

        }
        public ViewViewComponentResult Invoke()
        {
            LoginModel vm = new LoginModel();
            int userId = manager.GetCurrentUser();
            if (userId != 0)
            {
                vm.Admin = _service.GetById(userId);

                vm.status = true;
                return View(vm);
            }
            else 
            {

                vm.status = false;
                return View(vm);
            }
          
        
        }
    }
}
