﻿function callNotifybootstrap(title, delay) {
    $.notify({
        title: title
    },
        {
            placement: {
                from: "top",
                align: "right"
            },
            type: 'minimalist',
            delay: delay,
            allow_dismiss: false,
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-danger" role="alert">' +
                '<div class="row">' +
                '<span class="title" data-notify="title">{1}</span></div>' +
                '</div>'
        });
}
function callNotify(title, delay) {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    toastr.success(title);
}

function proccessBlocuk() {
    mApp.blockPage({
        overlayColor: "#000000",
        type: "loader",
        state: "primary",
        message: "Kayıt Ediliyor..."
    })
}

function proccessBlocukDiv(divname) {
    mApp.block("." + divname, {
        overlayColor: "#000000",
        type: "loader",
        state: "primary",
        message: "Kayıt Ediliyor..."
    })
}

function datepickerfunc(datepickerid) {
    $.fn.datetimepicker.dates['tr'] = {
        days: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi", "Pazar"],
        daysShort: ["Paz", "Pzt", "Sal", "Çar", "Per", "Cum", "Cmt", "Paz"],
        daysMin: ["Pa", "Pt", "Sl", "Ça", "Pe", "Cu", "Ct"],
        months: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        monthsShort: ["Ock", "Şbt", "Mar", "Nis", "May", "Haz", "Tem", "Ağu", "Eyl", "Eki", "Kas", "Ara"],
        today: "Bugün"
    };
    let current_datetime = new Date();
    let formatted_date = current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes();
    $(datepickerid).datetimepicker({
        format: "dd/mm/yyyy hh:ii",
        autoclose: true,
        todayBtn: true,
        minuteStep: 10,
        pickerPosition: "bottom-right",
        startDate: formatted_date,
        showMeridian: false,
        rtl: true
    });
    //$(datepickerid).datetimepicker('startDate', new Date());
}