﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dashboard.Utilities
{
    public class StringHelpers
    {
        public const string Save = "Kaydet";
        public const string Delete = "Sil";
        public const string Add = "Ekle";
        public const string Edit = "Düzenle";
        public const string Browse = "Gözat";
        public const string Passive = "Pasif";
        public const string Active = "Aktif";

        public const string AddImage = "Resim Ekle";
    }
}