﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Text.RegularExpressions;
//using System.Net;
//using System.IO;
//using iTextSharp.text;
//using iTextSharp.text.pdf;
//using iTextSharp.tool.xml;
//using iTextSharp.text.html.simpleparser;
//using System.Web.UI.WebControls;
//using System.Web.UI;
//using System.Web.Mvc;
//using System.Data;
//using Microsoft.AspNetCore.Http;

//namespace Dashboard.Utilities
//{
//    public class Helpers
//    {
//        public const string ThemeImages = "/Content/images";
//        public const string CdnUrl = "http://admin.canyayinlari.com/Upload/Images/";
//        public const string CdnBookUrl = "http://admin.canyayinlari.com/Upload/BooksImage/";
//        public const string CdnAuthorUrl = "http://admin.canyayinlari.com/Upload/PeopleImage/";

//        public static Boolean IsNumeric(object value)
//        {
//            Regex desen = new Regex("^[0-9]*$");
//            return desen.IsMatch(value.ToString());
//        }

//        public static bool LinkExists(string imageUrlAddress)
//        {
//            WebRequest webRequest = WebRequest.Create(imageUrlAddress);
//            WebResponse webResponse;
//            try
//            {
//                webResponse = webRequest.GetResponse();
//            }
//            catch
//            {
//                return false;
//            }
//            return true;
//        }

//        public static string GetIP()
//        {
//            string strHostName = "";
//            strHostName = Dns.GetHostName();

//            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);

//            IPAddress[] addr = ipEntry.AddressList;

//            return addr[addr.Length - 1].ToString();
//        }

//        /// <summary>
//        /// Get current user ip address.
//        /// </summary>
//        /// <returns>The IP Address</returns>
//        public static string GetUserIPAddress()
//        {
//            string ipadresi = String.Empty;

//            if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
//            {
//                ipadresi = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
//            }
//            else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
//            {
//                ipadresi = HttpContext.Current.Request.UserHostAddress;
//            }

//            return ipadresi;
//        }
//    }
//}