﻿
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;


namespace Dashboard.Utilities
{
    public class DataManager
    {
        private readonly IWebHostEnvironment webHostEnvironment;

     
        // private static FtpClient CreateFtpClient()
        // {
        //     var FTPClient = new FtpClient("waws-prod-am2-329.ftp.azurewebsites.windows.net",new System.Net.NetworkCredential{UserName=@"boodioapi\$boodioapi",Password="oGJdNYGiEipbodjihFbpvFjxpRtvuwprb5vqfpkHvXPpcwdWbQBBctbNRFdn"});
        //
        //     // FTPClient.AutoDetect();
        //     FTPClient.EncryptionMode=FtpEncryptionMode.Explicit;
        //     FTPClient.DataConnectionEncryption = true;
        //     FTPClient.DataConnectionType = FtpDataConnectionType.PASV;
        //     FTPClient.AutoDetect();
        //     FTPClient.Connect();
        //     return FTPClient;
        //         
        // }
        public DataManager(IWebHostEnvironment hostEnvironment)
        {
            webHostEnvironment = hostEnvironment;
        }

        public static string GenerateSeoLink(string phrase)
        {
            string str = phrase.ToLower();

            str = str.Replace("ç", "c").Replace("ı", "i").Replace("ş", "s").Replace("ğ", "g").Replace("ö", "o").Replace("ü", "u");
            str = str.Trim();
            str = str.Replace("-", "+");
            str = str.Replace(" ", "+");

            str = new System.Text.RegularExpressions.Regex("[^a-zA-Z0-9+]").Replace(str, "");
            str = str.Trim();
            str = str.Replace("+", "-");

            //// invalid chars, make into spaces
            //str = Regex.Replace(str, @"[^a-z0-9s-]", "-");

            //// hyphens
            ////str = Regex.Replace(str, @"s", "-");
            //// convert multiple spaces/hyphens into one space
            ////str = Regex.Replace(str, @"[s-]+", " ").Trim();

            //// cut and trim it
            ////str = str.Substring(0, str.Length <= 100 ? str.Length : 100).Trim();

            return str;
        }

        public string UploadedFile(IFormFile file, string path)
        {
            string fileName = null;


            if (file != null)
            {
                
                var extention = Path.GetExtension(file.FileName);
               ;
                fileName =  Guid.NewGuid().ToString()+extention;
               var rootPath = /*webHostEnvironment.ContentRootPath*/@"/site/wwwroot/";
                var finalPath = rootPath + path;
                if (!Directory.Exists(finalPath))
                {
                    Directory.CreateDirectory(finalPath);
                }
                var Upload = Path.Combine(finalPath, fileName);
             file.CopyTo(new FileStream(Upload, FileMode.Create));

                
                
                // using (FtpClient ftp=CreateFtpClient())
                // {
                //     if (!ftp.DirectoryExists(finalPath))
                //     {
                //         ftp.CreateDirectory(finalPath);
                //     }
                //     ftp.Upload(file.OpenReadStream(), Upload);
                //    
                //
                //
                //  
                //
                //
                //
                // }           
                
                
            }

            return fileName;


        }
      
        public string UploadedFileV1(IFormFile file, string path,string filename)
        {
            string fileName = null;
            path = "/wwwroot/" + path;

            if (file != null)
            {
                var extention = Path.GetExtension(file.FileName);

                fileName = filename + extention;
            
                var rootPath = webHostEnvironment.ContentRootPath;
                var finalPath = rootPath + path;
                if (!Directory.Exists(finalPath))
                {
                    Directory.CreateDirectory(finalPath);
                }
                var Upload = Path.Combine(finalPath, fileName);
                file.CopyTo(new FileStream(Upload, FileMode.Create));
                file.OpenReadStream().Dispose();
                file.OpenReadStream().Close();
                
               
                
                // using (FtpClient ftp=CreateFtpClient())
                // {
                //     if (!ftp.DirectoryExists(finalPath))
                //     {
                //         ftp.CreateDirectory(finalPath);
                //     }
                //     ftp.Upload(file.OpenReadStream(), Upload);
                //    
                //
                //
                //  
                //
                //
                //
                // }            
            }

            return fileName;


        }
        
        public string UploadedFileV4(IFormFile file, string path, int langId)
        {
            string fileName = null;
            string final = null;


            if (file != null)
            {
                var extention = Path.GetExtension(file.FileName);

                fileName = file.FileName.ToString() + extention;
                var rootPath = /*webHostEnvironment.ContentRootPath*/@"c:\vhosts\boodio.net\subdomains\api\";
                var finalPath = rootPath + path;


                if (!Directory.Exists(finalPath))
                {
                    Directory.CreateDirectory(finalPath);
                }
                var Upload = Path.Combine(finalPath, fileName);
                final = finalPath;
                file.CopyTo(new FileStream(Upload, FileMode.Create));

                file.OpenReadStream().Dispose();
                file.OpenReadStream().Close();
                return fileName;



            }

            return null;


        }

        public string UploadedFileV2(IFormFile file, string path,int langId)
        {
            string fileName = null;
            string final = null;


            if (file != null)
            {
              var extention=  Path.GetExtension(file.FileName);
               
                    fileName = langId.ToString()+extention;
                    var rootPath = /*webHostEnvironment.ContentRootPath*/@"c:\vhosts\boodio.net\subdomains\api\";
                    var finalPath = rootPath + path;


                    if (!Directory.Exists(finalPath))
                    {
                        Directory.CreateDirectory(finalPath);
                    }
                    var Upload = Path.Combine(finalPath, fileName);
                    final = finalPath;
                    file.CopyTo(new FileStream(Upload, FileMode.Create));
                 
                    file.OpenReadStream().Dispose();
                    file.OpenReadStream().Close();
                    return fileName;
           

               
            }

            return null;


        }
   
    }
}