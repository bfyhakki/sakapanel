﻿
using Business.Abstract;
using Core.Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Dashboard.Utilities
{
    public  class CookieManager
    {

        private readonly IHttpContextAccessor a;
        IAdminService adminService;
        public CookieManager(IHttpContextAccessor _a,IAdminService _adminService)
        {
            a = _a;

            adminService = _adminService;
        }

   
        public  void SetUser(int userId)
        {

            
            CookieOptions cookie = new CookieOptions();
            
            cookie.Expires = DateTime.Now.AddYears(10);
            
            a.HttpContext.Response.Cookies.Append("UserLoginCript", Utilities.CryptographyManager.StringCipher.Encrypt(userId.ToString()),cookie);
         
        }


        public  int GetCurrentUser()
        {
            var cookie = a.HttpContext.Request.Cookies["UserLoginCript"];
           bool state= a.HttpContext.Request.Cookies.ContainsKey("UserLoginCript");
            //CookieOptions cookie = new CookieOptions();
            //Request.Cookies["username"];

            if (cookie != null)
            {

                string cookieUser = Utilities.CryptographyManager.StringCipher.Decrypt(cookie.ToString());
                int userId = Convert.ToInt32(cookieUser);
                Admin user = new Admin();
                user =adminService.GetById(userId);
                return user != null ? user.AD_ID : 0;
            }

            return 0;
        }




        public  bool ClearCookie()
        {
            //var loginCookie = new HttpCookie("ui_al");
            //loginCookie.Expires = DateTime.Now.AddDays(-1);
            //HttpContext.Current.Response.Cookies.Add(loginCookie);
            //HttpContext.Current.Request.Cookies.Clear();
            //HttpContext.Current.Session.Abandon();
            a.HttpContext.Response.Cookies.Append("UserLoginCript", "", new CookieOptions()
            {
                Expires = DateTime.Now.AddDays(-1)
            });
            return GetCurrentUser() == 0 ? true : false;
        }

    }
}