﻿"use strict";
// Class definition

var KTDatatableHtmlTableDemo = function () {
	// Private functions

	// demo initializer
	var demo = function () {

		var datatable = $('#booklist').KTDatatable({
			data: {
				saveState: { cookie: false },
			},
			search: {
				input: $('#generalSearch'),
			},
			columns: [
				{
					field: 'DepositPaid',
					type: 'number',
				},
				{
					field: 'OrderDate',
					type: 'date',
					format: 'YYYY-MM-DD',
				},

				{
					field: 'Durum',
					title: 'Durum',

					autoHide: false,
					// callback function support for column rendering
					template: function (row) {
						var status = {
							1: { 'title': 'Yayında', 'class': 'kt-badge--success' },
							0: { 'title': 'Taslak', 'class': ' kt-badge--danger' },


						};
						return '<span class="kt-badge ' + status[row.Durum].class + ' kt-badge--inline kt-badge--pill">' + status[row.Durum].title + '</span>';
					},
				}, {
					field: 'Tip',
					title: 'Tip',
					autoHide: false,
					// callback function support for column rendering
					template: function (row) {
						var status = {
							1: { 'title': 'Kitap', 'state': 'success' },
							2: { 'title': 'Diğer', 'state': 'primary' },

						};
						return '<span class="kt-badge kt-badge--' + status[row.Tip].state + ' kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-' + status[row.Tip].state + '">' + status[row.Tip].title + '</span>';
					},

				},
				{
					field: 'İşlemler',
					title: 'İşlemler'

				},

			],
		});


		$('#kt_form_status').on('change', function () {
			datatable.search($(this).val().toLowerCase(), 'Durum');
		});

		$('#kt_form_type').on('change', function () {
			datatable.search($(this).val().toLowerCase(), 'Tip');
		});
		$('#kt_select2_1').on('change', function () {
			datatable.search($(this).val().toLowerCase(), 'Yayınevi');
		});
		$('#yazarlar').on('change', function () {
			datatable.search($(this).val().toLowerCase(), 'Yazarlar');
		});
		$('#kt_form_status,#kt_form_type,#kt_form_publisher').selectpicker();

	};
	var demo2 = function () {


		var datatable2 = $('#basic').KTDatatable({
			data: {
				saveState: { cookie: false },
			},
			search: {
				input: $('#generalSearch'),
			},
			columns: [
				{
					field: 'ID',
					type: 'number',
				},
				{
					field: 'Kategori Adı',

				},
				{
					field: 'İşlemler',
					title: 'İşlemler'

				},
				{
					field: 'Kitap Sayısı',
					title: 'İşlemler'

				},
				{
					field: 'Kitap Sayısı',
					title: 'İşlemler'

				},
				{
					field: 'Ayarlar',
					title: 'İşlemler'

				},


			],
		});

		$('#kt_form_status').on('change', function () {
			datatable2.search($(this).val().toLowerCase(), 'Durum');
		});

		
		$('#kt_form_status,#kt_form_type,#kt_form_publisher').selectpicker();

	};
	
	return {
		// Public functions
		init: function () {
			// init dmeo
			demo();
			demo2();
		
		},
	};
}();

jQuery(document).ready(function () {

	KTDatatableHtmlTableDemo.init();
});






