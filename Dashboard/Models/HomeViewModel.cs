﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;
using Microsoft.AspNetCore.Mvc;

namespace Dashboard.Models
{
    public class HomeViewModel
    {

        public List<Banner> Banners { get; set; }
        public List<ContactForm> ContactForms { get; set; }
        public List<Log> Logs { get; set; }
        public List<Customer> Customers { get; set; }
        public List<Order> Orders { get; set; }
        public int UsersCount { get; set; }
        public int BooksCount { get; set; }
        public int OrdersCount { get; set; }

     




    }
}