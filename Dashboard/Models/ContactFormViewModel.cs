using System.Collections.Generic;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;

namespace Dashboard.Models
{
    public class ContactFormViewModel
    {
      
   
        public List<ContactForm> ContactForms { get; set; }
      
        public ContactForm ContactForm { get; set; }
     
     
        
        public IFormFile PG_Photo { get; set; }
       
      
    }
}