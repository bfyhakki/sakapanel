using System.Collections.Generic;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;

namespace Dashboard.Models
{
    public class AnnounceViewModel
    {
      
   
        public List<Announce> Announces { get; set; }
      
        public Announce Announce { get; set; }
     
     
        
        public IFormFile ANN_Photo { get; set; }
       
      
    }
}