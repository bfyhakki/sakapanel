using System.Collections.Generic;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;

namespace Dashboard.Models
{
    public class PageViewModel
    {
      
   
        public List<Page> Pages { get; set; }
      
        public Page Page { get; set; }
     
     
        
        public IFormFile PG_Photo { get; set; }
       
      
    }
}