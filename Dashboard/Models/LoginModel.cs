﻿using Core.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.Models
{
    public class LoginModel
    {
        public Admin Admin { get; set; }
        public bool status { get; set; }
        public string Email { get; set; }
        public bool RememberMe { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
