using System.Collections.Generic;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;

namespace Dashboard.Models
{
    public class CustomerViewModel
    {
      
   
        public List<Customer> Customers { get; set; }
      
        public Customer Customer { get; set; }
     
     
        
        public IFormFile PG_Photo { get; set; }
       
      
    }
}