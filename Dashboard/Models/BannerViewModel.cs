using System.Collections.Generic;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;

namespace Dashboard.Models
{
    public class BannerViewModel
    {
      
   
        public List<Banner> Banners { get; set; }
      
        public Banner Banner { get; set; }
     
        public int[] selectedBooks { get; set; }
        
        public IFormFile _categoryImage { get; set; }
        public IFormFile _bannerImage { get; set; }
        public IFormFile _bookImage { get; set; }
    }
}