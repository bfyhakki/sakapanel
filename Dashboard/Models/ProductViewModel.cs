using System.Collections.Generic;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;

namespace Dashboard.Models
{
    public class ProductViewModel
    {
      
   
        public List<Product> Products { get; set; }
      
        public Product Product { get; set; }
     
     
        
        public IFormFile PRD_Photo { get; set; }
        public IFormFile PRD_CatologPhoto { get; set; }
      
    }
}