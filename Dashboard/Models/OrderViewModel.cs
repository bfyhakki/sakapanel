using System.Collections.Generic;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;

namespace Dashboard.Models
{
    public class OrderViewModel
    {
      
   
        public List<Order> Orders { get; set; }
        public List<Customer> Customers { get; set; }
        public Customer Customer { get; set; }
        public Order Order { get; set; }

        public string username { get; set; }
   
     
     
        
      
    }
}