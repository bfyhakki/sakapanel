﻿﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Dashboard.Models;
using Business.Abstract;
using Newtonsoft.Json.Linq;
using Authorization = Dashboard.Filters.Authorization;

namespace Dashboard.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IBannerService _bannerService;
        private IOrderService _orderService;
        private ICustomerService _customerService;
        private ILogService _logService;
        private IContactFormService _contactFormService;
        
        public HomeController(IContactFormService contactFormService, ICustomerService customerService,ILogService logService,IOrderService orderService, IBannerService bannerService, ILogger<HomeController> logger)
        {
            _contactFormService = contactFormService;
            _customerService = customerService;
            _logService = logService;
            _logger = logger;
            _bannerService = bannerService;
            _orderService = orderService;
       
        }
        [TypeFilter(typeof(Authorization))]
       
        public IActionResult Index()
        {
            HomeViewModel vm = new HomeViewModel();
            vm.Orders = _orderService.GetList().Data;
            vm.Customers = _customerService.GetList().Data;
            vm.Logs = _logService.GetList().Data;
            vm.ContactForms = _contactFormService.GetList().Data;
            return View(vm);
        }
       

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
