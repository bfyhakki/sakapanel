﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Core.Entities.Concrete;
using Dashboard.Filters;
using Dashboard.Models;
using Dashboard.Utilities;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Admin = Core.Entities.Concrete.Admin;

namespace Dashboard.Controllers
{
    public class LoginController : Controller
    {
        IAdminService _service;
      
      
        CookieManager manager;
        private readonly  IHttpContextAccessor _httpContextAccessor;
        public LoginController(IAdminService service ,IHttpContextAccessor contextAccessor)
        {
            _service = service;
           this._httpContextAccessor = contextAccessor;
            manager = new CookieManager(this._httpContextAccessor, service);

        }
      

        public IActionResult Index()
        {
            return View();
        }
     
        [HttpPost]
        public IActionResult Login(LoginModel vm)
        {
            ViewData["message"] = null;
            if (String.IsNullOrEmpty(vm.UserName)&&String.IsNullOrEmpty(vm.Password))
            {
                ViewData["message"] = "Lütfen bilgilerinizi kontrol ediniz.";
                return RedirectToAction("Index");
            }
            else
            {
                var MD5Pass = Utilities.CryptographyManager.StringCipher.Encrypt(vm.Password);
                Admin user = _service.GetByEmailAndPassword(vm.Email, MD5Pass);
                if (user != null && manager.GetCurrentUser() == 0)
                {
                    manager.SetUser(user.AD_ID);
                    var asd = Get("UserLoginCript");
                    return RedirectToAction("Index", "Home");

                }
            }
           
          



         
            return RedirectToAction("Index","Home");
                
        

           

        }
        public string Get(string key)
        {
            return Request.Cookies[key];
        }
        public void Set(string key, string value, int? expireTime)
        {
            CookieOptions option = new CookieOptions();
            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddMinutes(expireTime.Value);
            else
                option.Expires = DateTime.Now.AddMilliseconds(10);
            Response.Cookies.Append(key, value, option);
        }
        public void Kill()
        {
            Response.Cookies.Append("UserLoginCript", "", new CookieOptions()
            {
                Expires = DateTime.Now.AddDays(-1)
            });
        }

        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
      
        public ActionResult Register(Admin vm)
        {


            vm.AD_Password = Utilities.CryptographyManager.StringCipher.Encrypt(vm.AD_Password);

            _service.Add(vm);
                    if ( manager.GetCurrentUser() == 0)
                    {
                        manager.SetUser(vm.AD_ID);
                        ViewBag.Result = "Kayıt Başarılı.";
                    }
            
                ViewBag.Result = "Email adresi daha önce kayıt edilmiş.";
            return View();
            }
        // public IActionResult LoginSide()
        // {
        //     Admin vm = new Admin();
        //   int userId= manager.GetCurrentUser();
        // if (userId != 0)
        //     {
        //         vm = _service.GetById(userId);
        //         TempData["status"] = true;
        //
        //         return PartialView("LoginSide", vm);
        //     }
        //     if (userId <= 0)
        //     {
        //
        //         TempData["status"] = false;
        //         return PartialView("LoginSide");
        //     }
        //     TempData["status"] = true;
        //     return PartialView("LoginSide");
        // }
     
        [HttpPost]
        [TypeFilter(typeof(Authorization))]
        public ActionResult Logout(LoginModel vm)
        {
            if (vm.Admin.AD_ID > 0)
            {
                Kill();
            }
            return RedirectToAction("Index", "Login");
        }
    }






}
