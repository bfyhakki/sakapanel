﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Core.Utilities.Results;
using Dashboard.Filters;
using Dashboard.Models;
using Dashboard.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dashboard.Controllers
{
    public class AnnounceController : Controller
    {
        IAnnounceService _AnnounceService;
      
        
        readonly IHttpContextAccessor _httpcontextService;
        public static IWebHostEnvironment _webHostEnvironment;

        public AnnounceController(IAnnounceService AnnounceService, IHttpContextAccessor httpcontextService,
            IWebHostEnvironment webHostEnvironment)
        {

            _AnnounceService = AnnounceService;

            _httpcontextService = httpcontextService;
            _webHostEnvironment = webHostEnvironment;
        }

        DataManager d = new DataManager(_webHostEnvironment);
        [TypeFilter(typeof(Authorization))]
        public IActionResult Index()

        {
            AnnounceViewModel vm = new AnnounceViewModel();
            vm.Announces = _AnnounceService.GetList().Data.Where(x=> x.ANN_Status>-1).ToList();
          

            return View(vm);
        }
        [TypeFilter(typeof(Authorization))]
        public IActionResult Create()
        {
          
          
            return View();
        }


        [HttpPost]
        [TypeFilter(typeof(Authorization))]
        public IActionResult Create(AnnounceViewModel vm)
        {
            IResult value2;
         //   var AnnounceId = vm.Announce.BNC_ID.ToString();
      
         
         try
         {
             //var AnnounceId = vm.Announce.BNC_ID.ToString();
       
             if (vm.ANN_Photo != null)
             {
                 var image= d.UploadedFileV1(vm.ANN_Photo, "uploads/",Guid.NewGuid().ToString());
                 vm.Announce.ANN_Photo = image;

                 // string blobpath = @"C:\vhosts\boodio.net\subdomains\api\uploads\books\"+bookId+@"\"+filename+".jpeg";
                 //  string blobname = "books/" + bookId + "/" + filename+".jpeg";
                 // _blobService.UploadFile(blobpath, blobname);

             }
           
             var value = _AnnounceService.Add(vm.Announce);
         
             if (value.ValidationResult != null)

             {

                 foreach (var item in value.ValidationResult.Errors)

                 {

                     ModelState.AddModelError(item.PropertyName, item.ErrorMessage);

                 }

                 return Json(new { success = 0 });

             }

             else
             {
               

                 return Json(new { success = 1,id =vm.Announce.ANN_ID });
             }


         }
         catch (Exception e)
         {
             return Json(new {bla = e.Message});
         }
        


          






        }


        [TypeFilter(typeof(Authorization))]
        public IActionResult Edit(int Id)
        {
            AnnounceViewModel vm = new AnnounceViewModel();
            vm.Announce = _AnnounceService.GetById(Id).Data;
          
            return View(vm);
        }
        

        [TypeFilter(typeof(Authorization))]
        [HttpPost]
        public IActionResult Edit(AnnounceViewModel vm)
        {
            IResult value2;
            var AnnounceId = vm.Announce.ANN_ID.ToString();
     
            if (vm.ANN_Photo != null)
            {
                var image= d.UploadedFileV1(vm.ANN_Photo, "uploads/",Guid.NewGuid().ToString());
                vm.Announce.ANN_Photo = image;

                // string blobpath = @"C:\vhosts\boodio.net\subdomains\api\uploads\books\"+bookId+@"\"+filename+".jpeg";
                //  string blobname = "books/" + bookId + "/" + filename+".jpeg";
                // _blobService.UploadFile(blobpath, blobname);

            }
       
            var value = _AnnounceService.Update(vm.Announce);

            if (value.ValidationResult != null)

            {

                foreach (var item in value.ValidationResult.Errors)

                {

                    ModelState.AddModelError(item.PropertyName, item.ErrorMessage);

                }

                return Json(new { success = 0 });

            }

            else
            {

                return Json(new { success = 1 });
            }



        }

        [TypeFilter(typeof(Authorization))]
        public IActionResult Delete(int AnnounceId)
        {
            int value = 0;
            int id = 0;
            if (int.TryParse(AnnounceId.ToString(), out value))
            {
             
                var gelen = _AnnounceService.GetById(value);
                gelen.Data.ANN_Status = -1;
                _AnnounceService.Update(gelen.Data);
                id = gelen.Data.ANN_ID;
                return Json(new { success = 1 });
            }
            else
            {
                return Json(new { success = 0 });
            }
        }
  

       
    }
}