﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Core.Utilities.Results;
using Dashboard.Filters;
using Dashboard.Models;
using Dashboard.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dashboard.Controllers
{
    public class LogController : Controller
    {
        ILogService _LogService;
      
        
        readonly IHttpContextAccessor _httpcontextService;
        public static IWebHostEnvironment _webHostEnvironment;

        public LogController(ILogService LogService, IHttpContextAccessor httpcontextService,
            IWebHostEnvironment webHostEnvironment)
        {

            _LogService = LogService;

            _httpcontextService = httpcontextService;
            _webHostEnvironment = webHostEnvironment;
        }

        DataManager d = new DataManager(_webHostEnvironment);
        [TypeFilter(typeof(Authorization))]
        public IActionResult Index()

        {
            LogViewModel vm = new LogViewModel();
            vm.Logs = _LogService.GetList().Data.Where(x=> x.LOG_Status>-1).ToList();
          

            return View(vm);
        }
       


      
  

       
    }
}