﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Core.Utilities.Results;
using Dashboard.Filters;
using Dashboard.Models;
using Dashboard.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dashboard.Controllers
{
    public class CustomerController : Controller
    {
        ICustomerService _CustomerService;
        ICustomerService _customerService;
      
        
        readonly IHttpContextAccessor _httpcontextService;
        public static IWebHostEnvironment _webHostEnvironment;

        public CustomerController(ICustomerService customerService,ICustomerService CustomerService, IHttpContextAccessor httpcontextService,
            IWebHostEnvironment webHostEnvironment)
        {
            _customerService = customerService;
            _CustomerService = CustomerService;

            _httpcontextService = httpcontextService;
            _webHostEnvironment = webHostEnvironment;
        }

        DataManager d = new DataManager(_webHostEnvironment);
        [TypeFilter(typeof(Authorization))]
        public IActionResult Index()

        {
            CustomerViewModel vm = new CustomerViewModel();
            vm.Customers = _CustomerService.GetList().Data.Where(x=> x.CST_Status>-1).ToList();
        

            return View(vm);
        }
       


     

        [TypeFilter(typeof(Authorization))]
        public IActionResult Edit(int Id)
        {
            CustomerViewModel vm = new CustomerViewModel();
            vm.Customer = _CustomerService.GetById(Id).Data;
     
            return View(vm);
        }
        

      
  

       
    }
}