﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Core.Utilities.Results;
using Dashboard.Filters;
using Dashboard.Models;
using Dashboard.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dashboard.Controllers
{
    public class FaqController : Controller
    {
        IFaqService _FaqService;
      
        
        readonly IHttpContextAccessor _httpcontextService;
        public static IWebHostEnvironment _webHostEnvironment;

        public FaqController(IFaqService FaqService, IHttpContextAccessor httpcontextService,
            IWebHostEnvironment webHostEnvironment)
        {

            _FaqService = FaqService;

            _httpcontextService = httpcontextService;
            _webHostEnvironment = webHostEnvironment;
        }

        DataManager d = new DataManager(_webHostEnvironment);
        [TypeFilter(typeof(Authorization))]
        public IActionResult Index()

        {
            FaqViewModel vm = new FaqViewModel();
            vm.Faqs = _FaqService.GetList().Data.Where(x=> x.FAQ_Status>-1).ToList();
          

            return View(vm);
        }
        [TypeFilter(typeof(Authorization))]
        public IActionResult Create()
        {
          
          
            return View();
        }


        [HttpPost]
        [TypeFilter(typeof(Authorization))]
        public IActionResult Create(FaqViewModel vm)
        {
            IResult value2;
         //   var FaqId = vm.Faq.BNC_ID.ToString();
      
         
         try
         {
             //var FaqId = vm.Faq.BNC_ID.ToString();
       
           
           
             var value = _FaqService.Add(vm.Faq);
         
             if (value.ValidationResult != null)

             {

                 foreach (var item in value.ValidationResult.Errors)

                 {

                     ModelState.AddModelError(item.PropertyName, item.ErrorMessage);

                 }

                 return Json(new { success = 0 });

             }

             else
             {
               

                 return Json(new { success = 1,id =vm.Faq.FAQ_ID });
             }


         }
         catch (Exception e)
         {
             return Json(new {bla = e.Message});
         }
        


          






        }


        [TypeFilter(typeof(Authorization))]
        public IActionResult Edit(int Id)
        {
            FaqViewModel vm = new FaqViewModel();
            vm.Faq = _FaqService.GetById(Id).Data;
          
            return View(vm);
        }
        

        [TypeFilter(typeof(Authorization))]
        [HttpPost]
        public IActionResult Edit(FaqViewModel vm)
        {
            IResult value2;
            var FaqId = vm.Faq.FAQ_ID.ToString();
     
          
       
            var value = _FaqService.Update(vm.Faq);

            if (value.ValidationResult != null)

            {

                foreach (var item in value.ValidationResult.Errors)

                {

                    ModelState.AddModelError(item.PropertyName, item.ErrorMessage);

                }

                return Json(new { success = 0 });

            }

            else
            {

                return Json(new { success = 1 });
            }



        }

        [TypeFilter(typeof(Authorization))]
        public IActionResult Delete(int FaqId)
        {
            int value = 0;
            int id = 0;
            if (int.TryParse(FaqId.ToString(), out value))
            {
             
                var gelen = _FaqService.GetById(value);
                gelen.Data.FAQ_Status = -1;
                _FaqService.Update(gelen.Data);
                id = gelen.Data.FAQ_ID;
                return Json(new { success = 1 });
            }
            else
            {
                return Json(new { success = 0 });
            }
        }
  

       
    }
}