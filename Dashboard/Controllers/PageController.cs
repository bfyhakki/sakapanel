﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Core.Utilities.Results;
using Dashboard.Filters;
using Dashboard.Models;
using Dashboard.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dashboard.Controllers
{
    public class PageController : Controller
    {
        IPageService _PageService;
      
        
        readonly IHttpContextAccessor _httpcontextService;
        public static IWebHostEnvironment _webHostEnvironment;

        public PageController(IPageService PageService, IHttpContextAccessor httpcontextService,
            IWebHostEnvironment webHostEnvironment)
        {

            _PageService = PageService;

            _httpcontextService = httpcontextService;
            _webHostEnvironment = webHostEnvironment;
        }

        DataManager d = new DataManager(_webHostEnvironment);
        [TypeFilter(typeof(Authorization))]
        public IActionResult Index()

        {
            PageViewModel vm = new PageViewModel();
            vm.Pages = _PageService.GetList().Data.Where(x=> x.PG_Status>-1).ToList();
          

            return View(vm);
        }
       

       


        [TypeFilter(typeof(Authorization))]
        public IActionResult Edit(int Id)
        {
            PageViewModel vm = new PageViewModel();
            vm.Page = _PageService.GetById(Id).Data;
          
            return View(vm);
        }
        

        [TypeFilter(typeof(Authorization))]
        [HttpPost]
        public IActionResult Edit(PageViewModel vm)
        {
            IResult value2;
            var PageId = vm.Page.PG_ID.ToString();
     
            if (vm.PG_Photo != null)
            {
                var image= d.UploadedFileV1(vm.PG_Photo, "uploads/",Guid.NewGuid().ToString());
                vm.Page.PG_Photo = image;

                // string blobpath = @"C:\vhosts\boodio.net\subdomains\api\uploads\books\"+bookId+@"\"+filename+".jpeg";
                //  string blobname = "books/" + bookId + "/" + filename+".jpeg";
                // _blobService.UploadFile(blobpath, blobname);

            }
       
            var value = _PageService.Update(vm.Page);

            if (value.ValidationResult != null)

            {

                foreach (var item in value.ValidationResult.Errors)

                {

                    ModelState.AddModelError(item.PropertyName, item.ErrorMessage);

                }

                return Json(new { success = 0 });

            }

            else
            {

                return Json(new { success = 1 });
            }



        }

      

       
    }
}