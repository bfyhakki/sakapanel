﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Core.Utilities.Results;
using Dashboard.Filters;
using Dashboard.Models;
using Dashboard.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dashboard.Controllers
{
    public class ContactFormController : Controller
    {
        IContactFormService _ContactFormService;
      
        
        readonly IHttpContextAccessor _httpcontextService;
        public static IWebHostEnvironment _webHostEnvironment;

        public ContactFormController(IContactFormService ContactFormService, IHttpContextAccessor httpcontextService,
            IWebHostEnvironment webHostEnvironment)
        {

            _ContactFormService = ContactFormService;

            _httpcontextService = httpcontextService;
            _webHostEnvironment = webHostEnvironment;
        }

        DataManager d = new DataManager(_webHostEnvironment);
        [TypeFilter(typeof(Authorization))]
        public IActionResult Index()

        {
            ContactFormViewModel vm = new ContactFormViewModel();
            vm.ContactForms = _ContactFormService.GetList().Data.Where(x=> x.CF_Status>-1).ToList();
          

            return View(vm);
        }
       

       


        [TypeFilter(typeof(Authorization))]
        public IActionResult Edit(int Id)
        {
            ContactFormViewModel vm = new ContactFormViewModel();
            vm.ContactForm = _ContactFormService.GetById(Id).Data;
          
            return View(vm);
        }
        

        [TypeFilter(typeof(Authorization))]
        [HttpPost]
        public IActionResult Edit(ContactFormViewModel vm)
        {
            IResult value2;
            var ContactFormId = vm.ContactForm.CF_ID.ToString();
     
       
            var value = _ContactFormService.Update(vm.ContactForm);

            if (value.ValidationResult != null)

            {

                foreach (var item in value.ValidationResult.Errors)

                {

                    ModelState.AddModelError(item.PropertyName, item.ErrorMessage);

                }

                return Json(new { success = 0 });

            }

            else
            {

                return Json(new { success = 1 });
            }



        }

      

       
    }
}