﻿// ﻿using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Threading.Tasks;
// using Business.Abstract;
// using Core.Utilities.Results;
// using Dashboard.Filters;
// using Dashboard.Models;
// using Dashboard.Utilities;
// using Microsoft.AspNetCore.Hosting;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
//
// namespace Dashboard.Controllers
// {
//     public class BannerController : Controller
//     {
//         IBannerService _BannerService;
//         IBannerBookService _BannerBookService;
//         IBannerCategoryService _BannerCategoryService;
//         private IBookService _bookService;
//         private IPublisherService _publisherService;
//         
//         readonly IHttpContextAccessor _httpcontextService;
//         public static IWebHostEnvironment _webHostEnvironment;
//
//         public BannerController(IPublisherService publisherService,IBannerService BannerService,IBannerBookService BannerBookService,IBookService bookService,IBannerCategoryService BannerCategoryService,IHttpContextAccessor httpcontextService, IWebHostEnvironment webHostEnvironment)
//         {
//             
//             _BannerService = BannerService;
//             _BannerBookService = BannerBookService;
//             _bookService = bookService;
//             _BannerCategoryService = BannerCategoryService;
//             _httpcontextService = httpcontextService;
//             _webHostEnvironment = webHostEnvironment;
//             _publisherService = publisherService;
//         }
//         DataManager d = new DataManager(_webHostEnvironment);
//         [TypeFilter(typeof(Authorization))]
//         public IActionResult Index()
//
//         {
//             BannerViewModel vm = new BannerViewModel();
//             vm.Banners = _BannerService.GetList().Data.Where(x=> x.BNN_Status>-1).ToList();
//             vm.BannerCategories = _BannerCategoryService.GetList().Data;
//
//             return View(vm);
//         }
//         [TypeFilter(typeof(Authorization))]
//         public IActionResult Create()
//         {
//             BannerViewModel vm = new BannerViewModel();
//             vm.BannerCategories = _BannerCategoryService.GetList().Data;
//             
//           
//             return View(vm);
//         }
//
//
//         [HttpPost]
//         [TypeFilter(typeof(Authorization))]
//         public IActionResult Create(BannerViewModel vm)
//         {
//             IResult value2;
//          //   var bannerId = vm.Banner.BNC_ID.ToString();
//          var cat = _BannerCategoryService.GetById(vm.Banner.BNN_CategoryID);
//          
//          try
//          {
//              //var bannerId = vm.Banner.BNC_ID.ToString();
//              Guid g = Guid.NewGuid();
//              string filename = g.ToString();
//              var current = "";
//              
//              vm.Banner.BNN_Image = "/Banner/dummy.png";
//              
//              var value = _BannerService.Add(vm.Banner);
//          
//              if (value.ValidationResult != null)
//
//              {
//
//                  foreach (var item in value.ValidationResult.Errors)
//
//                  {
//
//                      ModelState.AddModelError(item.PropertyName, item.ErrorMessage);
//
//                  }
//
//                  return Json(new { success = 0 });
//
//              }
//
//              else
//              {
//                  if (vm._bannerImage != null)
//                  {
//                     var image= d.UploadedFileV1(vm._bannerImage, "/Uploads/Banner/" +vm.Banner.BNN_CategoryID.ToString()+"/"+vm.Banner.BNN_ID.ToString()+"/",filename);
//                      vm.Banner.BNN_Image = "/Banner/"+vm.Banner.BNN_CategoryID.ToString()+"/"+vm.Banner.BNN_ID+"/"+image;
//                                            
//                      vm.Banner.BNN_Status = 0;
//                      _BannerService.Update(vm.Banner);
//                      // string blobpath = @"C:\vhosts\boodio.net\subdomains\api\uploads\books\"+bookId+@"\"+filename+".jpeg";
//                      //  string blobname = "books/" + bookId + "/" + filename+".jpeg";
//                      // _blobService.UploadFile(blobpath, blobname);
//
//                  }
//
//                  return Json(new { success = 1,id =vm.Banner.BNN_ID });
//              }
//
//
//          }
//          catch (Exception e)
//          {
//              return Json(new {bla = e.Message});
//          }
//         
//
//
//           
//
//
//
//
//
//
//         }
//
//
//         [TypeFilter(typeof(Authorization))]
//         public IActionResult Edit(int Id)
//         {
//             BannerViewModel vm = new BannerViewModel();
//             vm.Banner = _BannerService.GetById(Id).Data;
//             vm.BannerCategories = _BannerCategoryService.GetList().Data;
//             vm.BannerBooks = _BannerBookService.GetList().Data.Where(x=> x.BNB_BannerID==Id&&x.BNB_Status>-1).ToList();
//             vm.Books = _bookService.GetList().Data.Where(x=>x.BK_Status>0).ToList();;
//             vm.Publishers = _publisherService.GetList().Data;
//             return View(vm);
//         }
//         
//         [TypeFilter(typeof(Authorization))]
//
//         public IActionResult GetBookChapter(int bookId)
//         {
//
//             var list = _BannerBookService.GetList().Data.Where(x=> x.BNB_BannerID==bookId&&x.BNB_Status>-1).OrderBy(x=> x.BNB_Order).ToList();
//           BannerViewModel vm = new BannerViewModel();
//           vm.Books = _bookService.GetList().Data.Where(x=>x.BK_Status>0).ToList();
//           vm.BannerBooks = list;
//           vm.Publishers = _publisherService.GetList().Data;
//
//             return PartialView("_BookChaptersPartial",vm);
//         }
//         [TypeFilter(typeof(Authorization))]
//         [HttpPost]
//         public IActionResult AddChapter(BannerViewModel vm)
//         {
//             vm.BannerBook.BNB_Status = 1;
//         
//             var value = _BannerBookService.Add(vm.BannerBook);
//
//          
//
//
//             if (value.ValidationResult != null)
//
//             {
//
//                 foreach (var item in value.ValidationResult.Errors)
//
//                 {
//
//                     ModelState.AddModelError(item.PropertyName, item.ErrorMessage);
//
//                 }
//
//                 return Json(new { success = 0 });
//
//             }
//
//             else
//             {
//
//
//                 return Json(new { success = 1, chapterId=vm.BannerBook.BNB_ID});
//             }
//
//
//         }
//         [TypeFilter(typeof(Authorization))]
//         [HttpPost]
//         public IActionResult UpdateChapter(BannerViewModel vm)
//         {
//             var gelen = _BannerBookService.GetById(vm.BannerBook.BNB_ID);
//            
//             
//          
//             var value = _BannerBookService.Update(vm.BannerBook);
//
//
//
//
//             if (value.ValidationResult != null)
//
//             {
//
//                 foreach (var item in value.ValidationResult.Errors)
//
//                 {
//
//                     ModelState.AddModelError(item.PropertyName, item.ErrorMessage);
//
//                 }
//
//                 return Json(new { success = 0 });
//
//             }
//
//             else
//             {
//
//
//                 return Json(new { success = 1, langId = vm.BannerBook.BNB_ID });
//             }
//
//
//         }
//         [TypeFilter(typeof(Authorization))]
//         [HttpPost]
//         public IActionResult Edit(BannerViewModel vm)
//         {
//             IResult value2;
//             var bannerId = vm.Banner.BNN_ID.ToString();
//             Guid g = Guid.NewGuid();
//             string filename = g.ToString();
//           
//             if (vm._bannerImage != null)
//             {
//                 var image= d.UploadedFileV1(vm._bannerImage, "/Uploads/Banner/" +vm.Banner.BNN_CategoryID.ToString()+"/"+vm.Banner.BNN_ID.ToString()+"/",filename);
//                 vm.Banner.BNN_Image = "/Banner/"+vm.Banner.BNN_CategoryID.ToString()+"/"+vm.Banner.BNN_ID+"/"+image;
//
//                 // string blobpath = @"C:\vhosts\boodio.net\subdomains\api\uploads\books\"+bookId+@"\"+filename+".jpeg";
//                 //  string blobname = "books/" + bookId + "/" + filename+".jpeg";
//                 // _blobService.UploadFile(blobpath, blobname);
//
//             }
//        
//             var value = _BannerService.Update(vm.Banner);
//
//             if (value.ValidationResult != null)
//
//             {
//
//                 foreach (var item in value.ValidationResult.Errors)
//
//                 {
//
//                     ModelState.AddModelError(item.PropertyName, item.ErrorMessage);
//
//                 }
//
//                 return Json(new { success = 0 });
//
//             }
//
//             else
//             {
//
//                 return Json(new { success = 1 });
//             }
//
//
//
//         }
//
//         [TypeFilter(typeof(Authorization))]
//         public IActionResult Delete(int bannerId)
//         {
//             int value = 0;
//             int id = 0;
//             if (int.TryParse(bannerId.ToString(), out value))
//             {
//                 var bannerBooks = _BannerBookService.GetList().Data.Where(x => x.BNB_BannerID == bannerId).ToList();
//                 foreach (var item in bannerBooks)
//                 {
//                     item.BNB_Status = -1;
//                     _BannerBookService.Update(item);
//                 }
//                 var gelen = _BannerService.GetById(value);
//                 gelen.Data.BNN_Status = -1;
//                 _BannerService.Update(gelen.Data);
//                 id = gelen.Data.BNN_ID;
//                 return Json(new { success = 1 });
//             }
//             else
//             {
//                 return Json(new { success = 0 });
//             }
//         }
//         [TypeFilter(typeof(Authorization))]
//         public IActionResult DeleteChapter(int bannerId)
//         {
//             int value = 0;
//             int id = 0;
//             if (int.TryParse(bannerId.ToString(), out value))
//             {
//                 var gelen = _BannerBookService.GetById(value);
//                 gelen.Data.BNB_Status = -1;
//                 _BannerBookService.Update(gelen.Data);
//                 id = gelen.Data.BNB_BannerID;
//                 return Json(new { success = 1,langId=id });
//             }
//             else
//             {
//                 return Json(new { success = 0,langId=0 });
//             }
//         }
//         
//         [TypeFilter(typeof(Authorization))]
//         [HttpPost]
//         public IActionResult UpdateStatus(int? Id)
//         {
//             int value;
//             bool right = int.TryParse(Id.ToString(), out value);
//             if (right)
//             {
//                 try
//                 {
//                     var book = _BannerService.GetById(value);
//                     if (book.Data.BNN_Status==0)
//                     {
//                         book.Data.BNN_Status = 1;
//                         _BannerService.Update(book.Data);
//                         return Json(new { success = 1, durum = 1 });
//                     }
//                     if(book.Data.BNN_Status == 1)
//                     {
//                         book.Data.BNN_Status = 0;
//                         _BannerService.Update(book.Data);
//                         return Json(new { success = 1, durum = 0 });
//                     }
//                  
//                     return Json(new { success = 0, durum = "Bir şeyler ters gitti" });
//                 }
//                 catch (Exception)
//                 {
//
//                     return Json(new { success = 0 });
//                 }
//
//             }
//             else
//             {
//                 return Json(new { success = 0 });
//
//             }
//
//         }
//
//        
//     }
// }