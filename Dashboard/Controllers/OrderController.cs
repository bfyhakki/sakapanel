﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Core.Utilities.Results;
using Dashboard.Filters;
using Dashboard.Models;
using Dashboard.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dashboard.Controllers
{
    public class OrderController : Controller
    {
        IOrderService _OrderService;
        ICustomerService _customerService;
      
        
        readonly IHttpContextAccessor _httpcontextService;
        public static IWebHostEnvironment _webHostEnvironment;

        public OrderController(ICustomerService customerService,IOrderService OrderService, IHttpContextAccessor httpcontextService,
            IWebHostEnvironment webHostEnvironment)
        {
            _customerService = customerService;
            _OrderService = OrderService;

            _httpcontextService = httpcontextService;
            _webHostEnvironment = webHostEnvironment;
        }

        DataManager d = new DataManager(_webHostEnvironment);
        [TypeFilter(typeof(Authorization))]
        public IActionResult Index()

        {
            OrderViewModel vm = new OrderViewModel();
            vm.Orders = _OrderService.GetList().Data.Where(x=> x.ORD_Status>-1).ToList();
            vm.Customers = _customerService.GetList().Data;

            return View(vm);
        }
       


     

        [TypeFilter(typeof(Authorization))]
        public IActionResult Edit(int Id)
        {
            OrderViewModel vm = new OrderViewModel();
            vm.Order = _OrderService.GetById(Id).Data;
            vm.Customer = _customerService.GetById(vm.Order.ORD_CustomerID).Data;
            return View(vm);
        }
        

      
  

       
    }
}