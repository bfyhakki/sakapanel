﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Core.Utilities.Results;
using Dashboard.Filters;
using Dashboard.Models;
using Dashboard.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dashboard.Controllers
{
    public class ProductController : Controller
    {
        IProductService _ProductService;
      
        
        readonly IHttpContextAccessor _httpcontextService;
        public static IWebHostEnvironment _webHostEnvironment;

        public ProductController(IProductService ProductService, IHttpContextAccessor httpcontextService,
            IWebHostEnvironment webHostEnvironment)
        {

            _ProductService = ProductService;

            _httpcontextService = httpcontextService;
            _webHostEnvironment = webHostEnvironment;
        }

        DataManager d = new DataManager(_webHostEnvironment);
        [TypeFilter(typeof(Authorization))]
        public IActionResult Index()

        {
            ProductViewModel vm = new ProductViewModel();
            vm.Products = _ProductService.GetList().Data.Where(x=> x.PRD_Status>-1).ToList();
          

            return View(vm);
        }
        [TypeFilter(typeof(Authorization))]
        public IActionResult Create()
        {
          
          
            return View();
        }


        [HttpPost]
        [TypeFilter(typeof(Authorization))]
        public IActionResult Create(ProductViewModel vm)
        {
            IResult value2;
         //   var ProductId = vm.Product.BNC_ID.ToString();
      
         
         try
         {
             //var ProductId = vm.Product.BNC_ID.ToString();
       
             if (vm.PRD_Photo != null)
             {
                 var image= d.UploadedFileV1(vm.PRD_Photo, "uploads/",Guid.NewGuid().ToString());
                 vm.Product.PRD_Photo = image;

                 // string blobpath = @"C:\vhosts\boodio.net\subdomains\api\uploads\books\"+bookId+@"\"+filename+".jpeg";
                 //  string blobname = "books/" + bookId + "/" + filename+".jpeg";
                 // _blobService.UploadFile(blobpath, blobname);

             }
             if (vm.PRD_CatologPhoto != null)
             {
                 var image= d.UploadedFileV1(vm.PRD_CatologPhoto, "uploads/",Guid.NewGuid().ToString());
                 vm.Product.PRD_CatologPhoto = image;

                 // string blobpath = @"C:\vhosts\boodio.net\subdomains\api\uploads\books\"+bookId+@"\"+filename+".jpeg";
                 //  string blobname = "books/" + bookId + "/" + filename+".jpeg";
                 // _blobService.UploadFile(blobpath, blobname);

             }
             var value = _ProductService.Add(vm.Product);
         
             if (value.ValidationResult != null)

             {

                 foreach (var item in value.ValidationResult.Errors)

                 {

                     ModelState.AddModelError(item.PropertyName, item.ErrorMessage);

                 }

                 return Json(new { success = 0 });

             }

             else
             {
               

                 return Json(new { success = 1,id =vm.Product.PRD_ID });
             }


         }
         catch (Exception e)
         {
             return Json(new {bla = e.Message});
         }
        


          






        }


        [TypeFilter(typeof(Authorization))]
        public IActionResult Edit(int Id)
        {
            ProductViewModel vm = new ProductViewModel();
            vm.Product = _ProductService.GetById(Id).Data;
          
            return View(vm);
        }
        

        [TypeFilter(typeof(Authorization))]
        [HttpPost]
        public IActionResult Edit(ProductViewModel vm)
        {
            IResult value2;
            var ProductId = vm.Product.PRD_ID.ToString();
     
            if (vm.PRD_Photo != null)
            {
                var image= d.UploadedFileV1(vm.PRD_Photo, "uploads/",Guid.NewGuid().ToString());
                vm.Product.PRD_Photo = image;

                // string blobpath = @"C:\vhosts\boodio.net\subdomains\api\uploads\books\"+bookId+@"\"+filename+".jpeg";
                //  string blobname = "books/" + bookId + "/" + filename+".jpeg";
                // _blobService.UploadFile(blobpath, blobname);

            }
            if (vm.PRD_CatologPhoto != null)
            {
                var image= d.UploadedFileV1(vm.PRD_CatologPhoto, "uploads/",Guid.NewGuid().ToString());
                vm.Product.PRD_CatologPhoto = image;

                // string blobpath = @"C:\vhosts\boodio.net\subdomains\api\uploads\books\"+bookId+@"\"+filename+".jpeg";
                //  string blobname = "books/" + bookId + "/" + filename+".jpeg";
                // _blobService.UploadFile(blobpath, blobname);

            }
            var value = _ProductService.Update(vm.Product);

            if (value.ValidationResult != null)

            {

                foreach (var item in value.ValidationResult.Errors)

                {

                    ModelState.AddModelError(item.PropertyName, item.ErrorMessage);

                }

                return Json(new { success = 0 });

            }

            else
            {

                return Json(new { success = 1 });
            }



        }

        [TypeFilter(typeof(Authorization))]
        public IActionResult Delete(int productId)
        {
            int value = 0;
            int id = 0;
            if (int.TryParse(productId.ToString(), out value))
            {
             
                var gelen = _ProductService.GetById(value);
                gelen.Data.PRD_Status = -1;
                _ProductService.Update(gelen.Data);
                id = gelen.Data.PRD_ID;
                return Json(new { success = 1 });
            }
            else
            {
                return Json(new { success = 0 });
            }
        }
  

       
    }
}