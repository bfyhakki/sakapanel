﻿
using Business.Abstract;
using Core.Entities.Concrete;
using Dashboard.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Dashboard.Filters
{
    public class Authorization : ActionFilterAttribute
    {

        IAdminService _adminService;
        private readonly IHttpContextAccessor _httpcontextService;
         CookieManager _manager;
        public Authorization(IAdminService adminService, IHttpContextAccessor httpContext)
        {
            _adminService = adminService;
            _httpcontextService = httpContext;
        _manager = new CookieManager(_httpcontextService,_adminService);
        }
      

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            int userId = _manager.GetCurrentUser();
            Admin user = userId != 0 ? _adminService.GetById(userId) : null;
            if (user == null)
            {
                filterContext.Result = new RedirectResult("~/Login/Index");
            }
        }
    }
}